var restify = require('restify');
var redis = require('redis');
var crypto = require('crypto');

function handleMap(req, res, next) {
}

function handlePostLocations(req, res, next) {
	res.send(200);
	var client = redis.createClient();

	client.on('connect', function () {
		console.log('Redis connected for POST.');
		length = req.body.locations.length;
		console.log(length + " locations to process.");
		for (i = 0; i < length; i++) {
			var obj = req.body.locations[i];
			var cacheKey = obj.tracking_id + obj.access_code + obj.track_name;
			var hash = crypto.createHash('md5').update(cacheKey).digest('hex');

			console.log("Pushing " + JSON.stringify(obj) + " to " + cacheKey + " [" + hash + "]");
			client.lpush([hash, JSON.stringify(obj)], function (err, reply) {
				if (err != null) {
					console.log("Redis error: " + err);
				}
				console.log("Added item #: " + reply);
			});
		}
	});
	next();
}

function handleGetLocations(req, res, next) {
	var cacheKey = req.params.tracking_id + req.params.access_code + req.params.track_name;
	var hash = crypto.createHash('md5').update(cacheKey).digest('hex');

	var client = redis.createClient();

	client.on('connect', function () {
		console.log("Redis connected for GET.");
		client.lrange(hash, 0, 120, function (err, reply) {

			var locations = [];

			if (err != null) {
				res.send(500, 'An error occurred getting locations: ' + err);
				next();
				return;
			}

			for (i = 0; i < reply.length; i++) {
				var obj = JSON.parse(reply[i]);
				obj._id = i;
				locations.push(obj);
			}
			res.send(200, locations);
			next();
		});
	});
}

var server = restify.createServer();
server.pre(restify.pre.sanitizePath());
server.use(restify.bodyParser());

server.post('/locations', handlePostLocations);

server.get('/locations/:tracking_id/:access_code/:track_name', handleGetLocations)

server.get('/map', restify.serveStatic({
	directory: './public',
	file: 'map.html'
}));

server.get('/marker.png', restify.serveStatic({
	directory: './public',
	file: 'marker.png'
}));

server.get('/flag-r.png', restify.serveStatic({
	directory: './public',
	file: 'flag-r.png'
}));

server.get('/flag-g.png', restify.serveStatic({
	directory: './public',
	file: 'flag-g.png'
}));


server.get('/map/:tracking_id/:access_code/:track_name', restify.serveStatic({
	directory: './public',
	file: 'map.html'
}));

server.listen(3333, function () {
	console.log('%s listening at %s', server.name, server.url);
});
